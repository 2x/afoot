SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

CREATE DATABASE IF NOT EXISTS `freepost_freepost` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `freepost_freepost`;

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL,
  `hashId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `dateCreated` date NOT NULL,
  `read` tinyint(1) NOT NULL,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  `vote` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `parentUserId` int(11) DEFAULT NULL,
  `postId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8027 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `post`;
CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL,
  `hashId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `dateCreated` date NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` text COLLATE utf8_unicode_ci,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  `vote` int(11) NOT NULL,
  `commentsCount` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6566 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `remember_me`;
CREATE TABLE IF NOT EXISTS `remember_me` (
  `token` char(128) COLLATE utf8_unicode_ci NOT NULL,
  `userId` int(10) NOT NULL,
  `expires` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Used for user authentication (keep session alive for returning user)';

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `hashId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_notifications` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `isActive` tinyint(1) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `passwordResetToken` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwordResetTokenExpire` datetime DEFAULT NULL,
  `registered` datetime NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `about` varchar(10000) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=264 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `vote_comment`;
CREATE TABLE IF NOT EXISTS `vote_comment` (
  `vote` smallint(6) NOT NULL,
  `datetime` datetime NOT NULL,
  `commentId` int(11) NOT NULL,
  `userId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `vote_post`;
CREATE TABLE IF NOT EXISTS `vote_post` (
  `vote` smallint(6) NOT NULL,
  `datetime` datetime NOT NULL,
  `postId` int(11) NOT NULL,
  `userId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `hashId` (`hashId`), ADD KEY `IDX_9474526C10EE4CEE` (`parentId`), ADD KEY `IDX_9474526C251330C5` (`parentUserId`), ADD KEY `IDX_9474526CE094D20D` (`postId`), ADD KEY `IDX_9474526C64B64DCC` (`userId`), ADD KEY `created` (`created`), ADD KEY `dateCreated` (`dateCreated`), ADD KEY `isRead` (`read`), ADD KEY `vote` (`vote`);

ALTER TABLE `post`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `hashId` (`hashId`), ADD KEY `IDX_5A8A6C8D64B64DCC` (`userId`), ADD KEY `created` (`created`), ADD KEY `dateCreated` (`dateCreated`), ADD KEY `vote` (`vote`);

ALTER TABLE `remember_me`
  ADD PRIMARY KEY (`token`), ADD KEY `userId` (`userId`);

ALTER TABLE `user`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `hashId` (`hashId`), ADD UNIQUE KEY `username` (`username`), ADD UNIQUE KEY `email` (`email`), ADD UNIQUE KEY `passwordResetCode` (`passwordResetToken`);

ALTER TABLE `vote_comment`
  ADD PRIMARY KEY (`commentId`,`userId`), ADD KEY `IDX_1FC60DF46690C3F5` (`commentId`), ADD KEY `IDX_1FC60DF464B64DCC` (`userId`);

ALTER TABLE `vote_post`
  ADD PRIMARY KEY (`postId`,`userId`), ADD KEY `IDX_EDE89DBCE094D20D` (`postId`), ADD KEY `IDX_EDE89DBC64B64DCC` (`userId`);


ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8027;
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6566;
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=264;

ALTER TABLE `comment`
ADD CONSTRAINT `FK_9474526C10EE4CEE` FOREIGN KEY (`parentId`) REFERENCES `comment` (`id`),
ADD CONSTRAINT `FK_9474526C251330C5` FOREIGN KEY (`parentUserId`) REFERENCES `user` (`id`),
ADD CONSTRAINT `FK_9474526C64B64DCC` FOREIGN KEY (`userId`) REFERENCES `user` (`id`),
ADD CONSTRAINT `FK_9474526CE094D20D` FOREIGN KEY (`postId`) REFERENCES `post` (`id`);

ALTER TABLE `post`
ADD CONSTRAINT `FK_5A8A6C8D64B64DCC` FOREIGN KEY (`userId`) REFERENCES `user` (`id`);

ALTER TABLE `remember_me`
ADD CONSTRAINT `FK_userId` FOREIGN KEY (`userId`) REFERENCES `user` (`id`);

ALTER TABLE `vote_comment`
ADD CONSTRAINT `FK_1FC60DF464B64DCC` FOREIGN KEY (`userId`) REFERENCES `user` (`id`),
ADD CONSTRAINT `FK_1FC60DF46690C3F5` FOREIGN KEY (`commentId`) REFERENCES `comment` (`id`);

ALTER TABLE `vote_post`
ADD CONSTRAINT `FK_EDE89DBC64B64DCC` FOREIGN KEY (`userId`) REFERENCES `user` (`id`),
ADD CONSTRAINT `FK_EDE89DBCE094D20D` FOREIGN KEY (`postId`) REFERENCES `post` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
